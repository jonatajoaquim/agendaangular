<?php
    class AgendamentosValidation {
        private $post;
        private $agendamentosModel;

        function __construct($params) {
            $this->post = $params;
            $this->agendamentosModel = new AgendamentosModel();
        }

        function validarAgendamentoParaInsert($idUsuario, $nomeUsuario) {
            if (!getPostValido($this->post, "dtAgendamento") || strlen(getPostValido($this->post, "dtAgendamento")) < 10) {
                return array(false, "Data inválida.");
            }
            if (!getPostValido($this->post, "descricao")) {
                return array(false, "Descrição inválida.");
            }
            $where = " WHERE id IN (SELECT agendamento_id
                                      FROM agendamentos_usuarios
                                     WHERE usuario_id = ".$idUsuario."
                                       AND dt_agendamento = '".getPostValido($this->post, "dtAgendamento")."') ";
            if ($this->agendamentosModel->getAgendamento($where) != null) {
                return array(false, "O usuário ".$nomeUsuario." já possúi compromisso para a hora marcada.");
            }
            return array(true, "Agendamento válido.");
        }
    }
?>