<?php
    class UsuariosValidation {
        private $post;

        function __construct($params) {
            $this->post = $params;
        }

        function validarUsuarioParaLogin() {
            if (!getPostValido($this->post, "emailUsuario")) {
                return array(false, "Email de usuário inválido.");
            }
            if (!getPostValido($this->post, "senhaUsuario") || strlen(getPostValido($this->post, "senhaUsuario")) < 6) {
                return array(false, "A senha deve ter no mínimo 6 caracteres.");
            }
            return array(true, "Usuário válido.");
        }

        function validarUsuarioParaInsert() {
            if (!getPostValido($this->post, "nomeUsuario") || strlen(getPostValido($this->post, "nomeUsuario")) < 3) {
                return array(false, "Nome de usuário inválido.");
            }
            if (!getPostValido($this->post, "sobrenomeUsuario") || strlen(getPostValido($this->post, "sobrenomeUsuario")) < 3) {
                return array(false, "Sobrenome de usuário inválido.");
            }
            if (!getPostValido($this->post, "emailUsuario")) {
                return array(false, "Email de usuário inválido.");
            }
            if (!getPostValido($this->post, "senhaUsuario") || strlen(getPostValido($this->post, "senhaUsuario")) < 6) {
                return array(false, "A senha deve ter no mínimo 6 caracteres.");
            }
            return array(true, "Usuário válido.");
        }
    }
?>