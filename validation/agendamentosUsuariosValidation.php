<?php
    class agendamentosUsuariosValidation {
        private $post;
        private $agendamentosUsuariosModel;

        function __construct($params) {
            $this->post = $params;
            $this->agendamentosUsuariosModel = new AgendamentosUsuariosModel();
        }

        function validarAgendametoParaInsert() {
            if (!getPostValido($this->post, "codAgendamento")) {
                return array(false, "O código do agendamento é inválido.");
            }
            if (!getPostValido($this->post, "codUsuario")) {
                return array(false, "O código do usuário é inválido.");
            }
            $agendamentosUsuarios = $this->agendamentosUsuariosModel->getAgendamentoUsuario(" WHERE agendamento_id = '".getPostValido($this->post, "codAgendamento")."' AND usuario_id = '".getPostValido($this->post, "codUsuario")."'");
            if ($agendamentosUsuarios != null) {
                return array(false, "Este usuário já está vinculado a um compromisso para a data e hora informados.");   
            }

            return array(true, "Agendamento e usuário são válidos.");
        }
    }
?>