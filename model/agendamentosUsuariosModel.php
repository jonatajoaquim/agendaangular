<?php
    class AgendamentosUsuariosModel extends GenericModel {
        function getAddAgendamentosUsuarios($values){
            return $this->add($values, "agendamento_id,usuario_id", " `agendamentos_usuarios` ");
        }
        function getAgendamentosUsuarios($condicao){
        	$campos = " id,
			 agendamento_id,
			 (SELECT descricao
					FROM agendamentos
				 WHERE agendamentos.id = agendamento_id
			 ) AS descricao_agend,
			 (SELECT dt_agendamento
					FROM agendamentos
				 WHERE agendamentos.id = agendamento_id
			 ) AS dt_agendamentos,
			 usuario_id,
			 (SELECT nome
					FROM usuarios
				 WHERE usuarios.id = usuario_id
			 ) AS nome_usuario ";
            return $this->get($campos, " agendamentos_usuarios ", $condicao);
        }
    }
?>