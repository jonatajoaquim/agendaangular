<?php
    class GenericModel {
        private $conexao;
        public function __construct() {
            $db = new Database();
            $this->conexao = $db->getConnection();
        }
        public function get($campo, $tabela, $condicao) {
            $sql = " SELECT ".$campo." FROM ".$tabela.$condicao." ";
            $stmt = $this->conexao->prepare($sql);
            $stmt->execute();
            $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $rows;
        }
        public function add($valores, $fields, $tabela) {
            $sql = " INSERT INTO ".$tabela." (".$fields.") VALUES(".$valores.") ";
            $stmt = $this->conexao->prepare($sql);
            $stmt->execute();
            $rows = $stmt->rowCount();
            return $rows;
        }
    }
?>