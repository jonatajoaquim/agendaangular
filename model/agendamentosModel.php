<?php
    class AgendamentosModel extends GenericModel {
        function getAddAgendamento($values){
            return $this->add($values, "dt_agendamento,descricao", " `agendamentos` ");
        }
        function getAgendamento($condicao){
            return $this->get(" * ", " agendamentos ", $condicao);
        }
        function getUltimoAgendamento(){
            return $this->get(" MAX(id) AS id ", " agendamentos ", "");
        }
    }
?>