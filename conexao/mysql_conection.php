<?php
class Database
{
 private static $factory;
 private $database;

 public static function getFactory()
 {
  if (!self::$factory) {
   self::$factory = new Conexao();
  }
  return self::$factory;
 }

 public function getConnection() {
  if (!$this->database) {
   $options = array(PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ, PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING, PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
   $this->database = new PDO('mysql:host=localhost;dbname=agendaangular;port=3306','agendaangular', 'agendaangular', $options);
  }
  return $this->database;
 }
}
?>