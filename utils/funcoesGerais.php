<?php
    function getPostValido($post, $indice) {
    	if (isset($post[$indice]) && $post[$indice] != null && trim($post[$indice]) != "") {
    		return $post[$indice];
    	}
    	return false;
    }
    function validaValoresParaQuery($valores) {
    	foreach ($valores as $key => $value) {
			$valores[$key] = addslashes(strip_tags($value));
    	}
    	return $valores;
	}
?>