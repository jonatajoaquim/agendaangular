/*
Navicat MySQL Data Transfer

Source Server         : agendaangular
Source Server Version : 50626
Source Host           : localhost:3306
Source Database       : agendaangular

Target Server Type    : MYSQL
Target Server Version : 50626
File Encoding         : 65001

Date: 2016-04-01 01:13:14
*/
create database agendaangular;
use agendaangular;
CREATE USER `agendaangular`@`localhost` IDENTIFIED BY 'agendaangular';
GRANT ALL PRIVILEGES ON agendaangular.* To 'agendaangular'@'localhost' IDENTIFIED BY 'agendaangular';


SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for agendamentos
-- ----------------------------
DROP TABLE IF EXISTS `agendamentos`;
CREATE TABLE `agendamentos` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `dt_agendamento` datetime NOT NULL,
  `descricao` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of agendamentos
-- ----------------------------
INSERT INTO `agendamentos` VALUES ('1', '2016-03-29 21:42:53', 'Reunião');
INSERT INTO `agendamentos` VALUES ('2', '2016-03-30 21:43:40', 'Futebol');
INSERT INTO `agendamentos` VALUES ('4', '2016-03-31 01:00:00', 'teste');
INSERT INTO `agendamentos` VALUES ('12', '2016-03-31 03:00:00', 'Estudos');

-- ----------------------------
-- Table structure for agendamentos_usuarios
-- ----------------------------
DROP TABLE IF EXISTS `agendamentos_usuarios`;
CREATE TABLE `agendamentos_usuarios` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `agendamento_id` int(6) NOT NULL,
  `usuario_id` int(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_agendimentos_agend_usu` (`agendamento_id`),
  KEY `fk_usuarios_agend_usu` (`usuario_id`),
  CONSTRAINT `fk_agendimentos_agend_usu` FOREIGN KEY (`agendamento_id`) REFERENCES `agendamentos` (`id`),
  CONSTRAINT `fk_usuarios_agend_usu` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of agendamentos_usuarios
-- ----------------------------
INSERT INTO `agendamentos_usuarios` VALUES ('1', '1', '1');
INSERT INTO `agendamentos_usuarios` VALUES ('2', '1', '2');
INSERT INTO `agendamentos_usuarios` VALUES ('3', '1', '3');
INSERT INTO `agendamentos_usuarios` VALUES ('4', '2', '1');
INSERT INTO `agendamentos_usuarios` VALUES ('5', '2', '3');
INSERT INTO `agendamentos_usuarios` VALUES ('6', '4', '3');
INSERT INTO `agendamentos_usuarios` VALUES ('7', '4', '2');
INSERT INTO `agendamentos_usuarios` VALUES ('8', '12', '1');
INSERT INTO `agendamentos_usuarios` VALUES ('9', '12', '2');

-- ----------------------------
-- Table structure for usuarios
-- ----------------------------
DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `sobrenome` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `senha` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of usuarios
-- ----------------------------
INSERT INTO `usuarios` VALUES ('1', 'Jonata', 'de Miranda', 'ledesma@iluminati.com', 'abc123');
INSERT INTO `usuarios` VALUES ('2', 'Diana', 'Dutra', 'dianasc@gmail.com', 'abc123');
INSERT INTO `usuarios` VALUES ('3', 'Dilnei', 'Mazzuco', 'dilned@gmail.com', 'abc123');
