<?php
    include("../conexao/mysql_conection.php");
    include("../model/genericModel.php");
    include("../controller/usuariosController.php");
    include("../utils/funcoesGerais.php");
    $funcao = $_POST['funcao'];
    $params = $_POST['params'];
    call_user_func($funcao, $params['post']);

    function getAddUsuario($params) {
        $dados = array();
        $controllerUsuarios = new UsuariosController($params);
        $dados['usuarios'] = $controllerUsuarios->getAddUsuario(); 
        echo json_encode($dados);
    }
?>