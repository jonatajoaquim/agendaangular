<?php
    session_start();
    include("../conexao/mysql_conection.php");
    include("../model/genericModel.php");
    include("../controller/usuariosController.php");
    include("../utils/funcoesGerais.php");
    $funcao = $_POST['funcao'];
    $params = $_POST['params'];
    call_user_func($funcao, $params['post']);

    function logar($params) {
        $dados = array();
        $controllerUsuarios = new UsuariosController($params);
        $dados['usuarios'] = $controllerUsuarios->logar(); 
        if ($dados['usuarios'][0][1][0] == "success") {
            $_SESSION['logado'] = 'S';
            $_SESSION['idUsuario'] = $dados['usuarios'][1][0]['id'];
            $_SESSION['usuarioLogado'] = $dados['usuarios'][1][0]['email'];
        } else {
            session_destroy();
        }
        echo json_encode($dados);
    }
?>