<?php
    session_start();
    include("../conexao/mysql_conection.php");
    include("../model/genericModel.php");
    include("../controller/usuariosController.php");
    include("../controller/agendamentosController.php");
    include("../controller/agendamentosUsuariosController.php");
    include("../utils/funcoesGerais.php");
    $funcao = $_POST['funcao'];
    $params = isset($_POST['params']) ? $_POST['params'] : null;
    call_user_func($funcao, isset($params['post']) ? $params['post'] : null);

    function getSession() {
        if (isset($_SESSION['logado']) && $_SESSION['logado'] == 'S') {
            $dados['login']['logado'] = "S";
            $dados['login']['usuarioLogado'] = $_SESSION['usuarioLogado'];
        } else {
            $dados['login']['logado'] = "N";
            $dados['login']['usuarioLogado'] = "Não há usuário logado.";
        }
        echo json_encode($dados);
    }
    function logout() {
        session_destroy();
        $dados['login']['logout'] = "S";
        echo json_encode($dados);
    }
    function addAgendamento($params) {
        $dados = array();
        $controllerAgendamentos = new AgendamentosController($params);
        $dados['agendamento'] = $controllerAgendamentos->getAddAgendamento(); 
        $_SESSION['usuariosAdicionados'] = null;
        echo json_encode($dados);
    }
    function getListaUsuarios($params) {
        $dados = array();
        $controllerUsuarios = new UsuariosController($params);
        $dados['usuarios'] = $controllerUsuarios->getListaUsuarios(); 
        echo json_encode($dados);
    }
    function getListaAgendamentos($params) {
        $dados = array();
        $controllerAgendamentos = new AgendamentosController($params);
        $dados['agendamentos'] = $controllerAgendamentos->getListaAgendamentos(); 
        echo json_encode($dados);
    }
    function exibirParticipantesAgendamento($params) {
        $dados = array();
        $controllerAgendamentosUsuarios = new AgendamentosUsuariosController($params);
        $dados['agendamentosUsuarios'] = $controllerAgendamentosUsuarios->getAgendamentosUsuarios(); 
        echo json_encode($dados);
    }
    function setListaUsuariosSelecionados($params){
        $_SESSION['usuariosAdicionados'] = $params;
    }
?>