<?php
    include("../model/agendamentosUsuariosModel.php");
    include("../validation/agendamentosUsuariosValidation.php");
    
    class AgendamentosUsuariosController {
        private $agendamentosUsuariosModel;
        private $agendamentosUsuariosValidation;
        private $post;

        function __construct($params) {
            $this->agendamentosUsuariosModel = new AgendamentosUsuariosModel();
            $this->agendamentosUsuariosValidation = new AgendamentosUsuariosValidation($params);
            $this->post = $params;
        }

        function getAddAgendamentosUsuarios() {
            $agendamentosUsuariosValido = $this->agendamentosUsuariosValidation->validarAgendamentosUsuariosParaInsert();
            if ($agendamentoUsuariosValido[0]) {
                $valuesSeparadosPorVirgula = $this->getValuesSeparadosPorVirgula($this->post);
                $insert = $this->agendamentosUsuariosModel->getAddAgendamentosUsuarios($valuesSeparadosPorVirgula);
                if ($insert > 0) {
                    $message = array("message", array("success", "Registro inserido com sucesso."));
                } else {
                    $message = array("message", array("error", "Erro ao inserir registro."));
                }
            } else {
                $message = array("message", array("error", $agendamentosUsuariosValido[1]));
            }
            $dados = $message;
            return $dados;
        }

        function getAgendamentosUsuarios() {
            $post = $this->post;
            $select = $this->agendamentosUsuariosModel->getAgendamentosUsuarios(" WHERE agendamento_id = ".$post['id']);
            $dadosToReturn = "";
            if ($select != null) {
                $dadosToReturn = $select;
            } else {
                $dadosToReturn = array("message", array("error", "Erro: Não foram retornados dados."));
            }
            $dados = $dadosToReturn;
            return $dados;
        }

        function getValuesSeparadosPorVirgula($post) {
            $arrayValues = array();
            array_push($arrayValues, $post['agendamentoId']);
            array_push($arrayValues, $post['usuarioId']);
            return "'".implode("','", $arrayValues)."'";
        }
    }
?>