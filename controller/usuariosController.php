<?php
    include("../model/usuariosModel.php");
    include("../validation/usuariosValidation.php");
    
    class UsuariosController {
        private $usuariosModel;
        private $usuariosValidation;
        private $post;

        function __construct($params) {
            $this->usuariosModel = new UsuariosModel();
            $this->usuariosValidation = new UsuariosValidation($params);
            $this->post = $params;
        }

        function getAddUsuario() {
            $usuarioValido = $this->usuariosValidation->validarUsuarioParaInsert();
            if ($usuarioValido[0]) {
                $valuesSeparadosPorVirgula = $this->getValuesSeparadosPorVirgula($this->post);
                $insert = $this->usuariosModel->getAddUsuario($valuesSeparadosPorVirgula);
                if ($insert > 0) {
                    $message = array("message", array("success", "Registro inserido com sucesso."));
                } else {
                    $message = array("message", array("error", "Erro ao inserir registro."));
                }
            } else {
                $message = array("message", array("error", $usuarioValido[1]));
            }
            $dados = $message;
            return $dados;
        }

         function getUsuarios() {
            $select = $this->usuariosModel->getUsuario("");
            $dadosToReturn = "";
            if ($select != null) {
                $dadosToReturn = $select;
            } else {
                $dadosToReturn = array("message", array("error", "Erro: Não foram retornados dados."));
            }
            $dados = $dadosToReturn;
            return $dados;
        }

        function getListaUsuarios() {
            $select = $this->usuariosModel->getUsuario(" WHERE id <> ".$_SESSION['idUsuario']);
            $dadosToReturn = "";
            if ($select != null) {
                $dadosToReturn = $select;
            } else {
                $dadosToReturn = array("message", array("error", "Erro: Não foram retornados dados."));
            }
            $dados = $dadosToReturn;
            return $dados;
        }

        function logar() {
            $post = $this->post;
            $usuarioValido = $this->usuariosValidation->validarUsuarioParaLogin();
            if ($usuarioValido[0]) {
                $valoresQuery = array("emailUsuario"=>$post['emailUsuario'], "senhaUsuario"=>$post['senhaUsuario']);
                $valoresQueryValidos = validaValoresParaQuery($valoresQuery);
                $select = $this->usuariosModel->getUsuario(" WHERE email = '".$valoresQueryValidos['emailUsuario']."' AND senha = '".$valoresQueryValidos['senhaUsuario']."' ");
                if ($select != null) {
                    $message = array("message", array("success", "Login efetuado com sucesso."));
                } else {
                    $message = array("message", array("error", "Erro ao tentar logar. E-mail ou senha inválidos."));
                }
            } else {
                $message = array("message", array("error", $usuarioValido[1]));
            }

            $dados = array($message, $select);
            return $dados;
        }

        function getValuesSeparadosPorVirgula($post) {
            $arrayValues = array();
            array_push($arrayValues, $post['nomeUsuario']);
            array_push($arrayValues, $post['sobrenomeUsuario']);
            array_push($arrayValues, $post['emailUsuario']);
            array_push($arrayValues, $post['senhaUsuario']);
            return "'".implode("','", $arrayValues)."'";
        }
    }
?>