<?php
    include("../model/agendamentosModel.php");
    include("../validation/agendamentosValidation.php");

    
    class AgendamentosController {
        private $agendamentosModel;
        private $agendamentosValidation;
        private $agendamentosUsuariosModel;
        private $agendamentosUsuariosValidation;
        private $post;

        function __construct($params) {
            $this->agendamentosModel = new AgendamentosModel();
            $this->agendamentosValidation = new AgendamentosValidation($params);
            $this->agendamentosUsuariosModel = new AgendamentosUsuariosModel();
            $this->agendamentosUsuariosValidation = new AgendamentosUsuariosValidation($params);
            $this->post = $params;
        }

        function getAddAgendamento() {
            $agendamentoValido = $this->agendamentosValidation->validarAgendamentoParaInsert($_SESSION['idUsuario'], $_SESSION['usuarioLogado']);
            if (!$agendamentoValido[0]) {
                $message = array("message", array("error", $agendamentoValido[1]));
                return $message;
            }
            for ($i = 0; $i<count($_SESSION['usuariosAdicionados']); $i++) {
                $agendamentoValido = $this->agendamentosValidation->validarAgendamentoParaInsert($_SESSION['usuariosAdicionados'][$i][0], $_SESSION['usuariosAdicionados'][$i][1]);
                if (!$agendamentoValido[0]) {
                    $message = array("message", array("error", $agendamentoValido[1]));
                    return $message;
                }
            }
            $valuesSeparadosPorVirgula = $this->getValuesSeparadosPorVirgula($this->post);
            $insert = $this->agendamentosModel->getAddAgendamento($valuesSeparadosPorVirgula);
            if ($insert > 0) {
                $message = array("message", array("success", "Registro inserido com sucesso."));
            } else {
                $message = array("message", array("error", "Erro ao inserir registro."));
            }

            $ultimoAgendamento = $this->agendamentosModel->getUltimoAgendamento();
            $insert = $this->agendamentosUsuariosModel->getAddAgendamentosUsuarios($ultimoAgendamento[0]['id'].",".$_SESSION['idUsuario']);
            for ($i = 0; $i<count($_SESSION['usuariosAdicionados']); $i++) {
                $insert = $this->agendamentosUsuariosModel->getAddAgendamentosUsuarios($ultimoAgendamento[0]['id'].",".$_SESSION['usuariosAdicionados'][$i][0]);
            }

            $dados = $message;
            return $dados;
        }

        function getListaAgendamentos() {
            $select = $this->agendamentosModel->getAgendamento("");
            $dadosToReturn = "";
            if ($select != null) {
                $dadosToReturn = $select;
            } else {
                $dadosToReturn = array("message", array("error", "Erro: Não foram retornados dados."));
            }
            $dados = $dadosToReturn;
            return $dados;
        }

        function getValuesSeparadosPorVirgula($post) {
            $arrayValues = array();
            array_push($arrayValues, $post['dtAgendamento']);
            array_push($arrayValues, $post['descricao']);
            return "'".implode("','", $arrayValues)."'";
        }
    }
?>